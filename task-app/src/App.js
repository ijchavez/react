import './Styles/App.css';

import Logo from './Components/Logo';
import TaskList from './Components/TaskList';


function App() {
  return (
    <div className="App">
      <div className = 'logoContainer'>
        <Logo>Logo</Logo>

      </div>
      <div className = 'task-main-list'>
         <h1>My Tasks</h1>
         <TaskList />

      </div>
      
    </div>

  );
  
}

export default App;
