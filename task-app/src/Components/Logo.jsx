import React from 'react';
import logo from '../images/logo.png'

import '../Styles/logo.css';

function Logo(){
    return (
        <div className="logoContainer">
            <img className= "logo" 
              src = {logo}
              alt = 'logo'

            />
        
        </div>
    )
}

export default Logo;