import React, { useState } from 'react';
import { v4 as uuidv4} from 'uuid';

import '../Styles/taskform.css';

function TaskForm(props){

    const [input, setInput] = useState('');

    const handleChange = e => {
        setInput(e.target.value);

    }
    const handleShip = e => {
        e.preventDefault();
        const newTask = {
            id: uuidv4(),
            text: input,
            completed: false

        }
        props.onSubmit(newTask);

    }

    return (
        <form 
            className = 'task-form'
            onSubmit = {handleShip}>

            <input 
              className = 'input-task'
              type = 'text'
              placeholder = 'Write a task'
              name = 'text'
              onChange = {handleChange}

            />
            <button 
              className = 'button-task'>
                  Add Task
            </button>
        </form>
    )

} 

export default TaskForm;