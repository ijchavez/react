import React from 'react';
import '../Styles/Button.css';

function Button ({ text, isClickBtn, handleClick }){
    return  (
        <button 
            className = { isClickBtn ? 'click-btn' : 'reset-btn' }
            onClick = {handleClick} >
            {text}

        </button>

    )

}

export default Button