import './App.css';
import logo from './images/logo.png';
import Button from './components/button';
import Counter from './components/counter';
import { useState } from 'react'

function App() {
  const [clickNumber, setClickNumber] = useState(0);
  
  const handleClick = () => {
    setClickNumber(clickNumber + 1);

  }
  const resetCounter = () => {
    setClickNumber(0)
    
  }
  return (
    <div className="App">
      <div className="logo-container">
        <img className= "logo" 
          src = {logo}
          alt = 'logo'/>
      
      </div>
      <div className = 'main-container'>
          <Counter clickNumber = {clickNumber} />

          <Button 
            text = 'Click' 
            isClickBtn = {true}
            handleClick = {handleClick}

          />
          <Button 
            text = 'Reset' 
            isClickBtn = {false}
            handleClick = {resetCounter}

          />
      </div> 

    </div>

  );

}

export default App;
