# React

## Componente
Parte de la interfaz de usuario, independiente y reusable

### Funcionales
Funcion de JS que retorna un elemento de react (JSX)
    * Debe retornarn un elemento JSX
    * Debe comenzar con una letra mayúscula
    * Puede recibir valores si es necesario

### de Clase
Clase de ES6 que retorna un elemento JSX
    * Debe extender React.Component
    * Debe tener un metodo render para retornar un elemento JSX
    * Puede recibir valores si es necesario

Los props solo pueden ser enviados de padre a hijo

Estado >> representación en JS del conjunto de propiedades de un componente y sus valores actuales
Hook >> Función especial que te permite trabajar con estados en componentes funcionales y otros aspectos de React sin escribir
un componente de clase para hacer código más conciso y mas fácil de entender

Event Listener >> fn de java que se ejecuta cuando ocurre un evento específico

Elemento >> unidades más pequeñas en React. Definen lo que se ve en pantalla

Elemento dentro de otro dentro el HTML

```
import ReactDom from 'react-dom';

const elemento = <h1> Hola Mundo </h1>;
ReactDom.render(
    elemento,
    document.getElementById('root')

);
```