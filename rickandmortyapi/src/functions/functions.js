import axios from 'axios'

const baseUrl = "https://rickandmortyapi.com/api";
const basePath = "/character/";


const allCharacters = async (state) => {
    const petition = await axios.get(baseUrl + basePath)
    state(petition.data.results);


}
const selectCharacter = async (id, state) => {
    const petition = await axios.get(baseUrl + basePath + `${id}`)
    state(petition.data);
    
}
export {
    allCharacters,
    selectCharacter

}