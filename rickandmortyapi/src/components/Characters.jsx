import React, { useEffect, useState }  from 'react'
import { useParams } from 'react-router-dom'
import { selectCharacter } from '../functions/functions'

import { Button, ListGroup } from 'react-bootstrap'

const Characters = () => {
  const [characters, setCharacters] = useState(null);
  const params = useParams();
  
  useEffect(() => {
    selectCharacter(params.id, setCharacters);

  }, [])
  return (
    <>
      <Button href="/">Go Back</Button>

      {characters !== null ? (
        <div className = "character">
          <h2>ID: {params.id}</h2>
          <img src = {characters.image} alt='' />
        
          <p>Full Name: {characters.name}</p>
          <p>Status: {characters.status}</p>
          <p>Species: {characters.species}</p>
          <p>Origin: {characters.origin.name}</p>
          <h6>Episodes:</h6>
        </div>
        

      ) : ('No character found')}

    </>
              
  )
  
}

export default Characters