import React, { useEffect, useState } from 'react'
import { allCharacters } from '../functions/functions'
import { ListGroup } from 'react-bootstrap'

const Start = () => {
  const [ characters, setCharacters ] = useState(null)

  useEffect(() => {
    allCharacters(setCharacters)

  }, [])

  return (
    <>
      {characters !== null ? (
        characters.map(character => (
            <ListGroup>
              <ListGroup.Item>
                <div key = {character.id}>
                    <a href={`/character/${character.id}`}>{character.name}</a>

                </div>
              </ListGroup.Item>
            </ListGroup>

          )

        )
      ) : ('No characters')}
    </>

  )
  
}

export default Start