import React from "react";
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Start from './components/Start';
import Characters from './components/Characters';

import "bootstrap/dist/css/bootstrap.min.css";
import './Styles/App.css';

function App() {
  return (
    <div className="App" >
      <BrowserRouter>
         <Routes>
           <Route path="/" element = {<Start></Start>}></Route>
           <Route path="/character/:id" element = {<Characters></Characters>}></Route>
         </Routes>
        
      </BrowserRouter>
    </div>

  );

}

export default App;
