import React, {useEffect, useState} from 'react';
import { airportInfo } from '../functions/functions';
import { Button, FloatingLabel, Form } from 'react-bootstrap'

let userValue = "EZE";

const getInputValue = (event)=>{
    userValue = event.target.value;

};
function clickOnSearchBtn(){
    console.log(">>> " + userValue)
    return userValue;
    
}
const Start = () => {
    const[airport, setAirport] = useState(null);
    
    useEffect(() => {
        airportInfo(setAirport, clickOnSearchBtn());

    }, [])
    return (
        <>
            <FloatingLabel
                controlId="floatingInput"
                label="Airport Code (iata)"
                className="mb-3 formplace">
                <Form.Control 
                    type="text" 
                    placeholder='EZE'
                    onChange={getInputValue}
                    />

                <Button 
                    variant="primary" 
                    as="input" 
                    type="submit" 
                    value="Search"
                    onClick = {clickOnSearchBtn}
                    />


            </FloatingLabel>
            {airport != null ? (
                <div>
                    <p>ID: {airport.id}</p>
                    <p>iata: {airport.iata}</p>
                    <p>icao: {airport.icao}</p>
                    <p>Name: {airport.name}</p>
                    <p>Location: {airport.location}</p>
                    <p>Adress:</p>
                        <div>
                            <p>{airport.street_number} {airport.street}</p>
                            <p>{airport.postal_code} {airport.location} {airport.city}</p>
                            <p>{airport.county} {airport.state}</p>

                        </div>
                    <div>
                        <p>Country: {airport.country_iso}, {airport.country}</p>

                    </div>
                    <p>Phone: {airport.phone}</p>
                    <p>GPS Coordenates: {airport.latitude}, {airport.longitude}</p>
                    <p>ID: {airport.uct}</p>
                    <p>Website:
                            <a href={airport.website}>{airport.website}</a>
                    </p>

                </div>
            ) : ('no airport found with iata provided')}
             
        </>

    )

}

export default Start