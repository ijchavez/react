import React from 'react';

import './Styles/App.css';

import Button from './components/Button';
import Screen from './components/Screen';
import ClearButton from './components/ClearButton';
import Logo from './components/Logo';


import { evaluate } from 'mathjs';
import { useState } from 'react';

const emptyValueErrMsg = "Por favor ingrese valores para realizar los cálculos.";

function App() {
  const [input, setInput] = useState('');

  const addInput = value => {
      setInput(input + value);

  };
  const getResult = () => {
    if(input) {
      setInput(evaluate(input));

    } else {
      alert(emptyValueErrMsg);

    }

  }
  return (
    <div className="App">
        <Logo>Logo</Logo>
        <div className = 'calculator-container'>
          <Screen input = {input} />

          <div className = 'fila'>
            <Button handleClick = {addInput}>1</Button>
            <Button handleClick = {addInput}>2</Button>
            <Button handleClick = {addInput}>3</Button>
            <Button handleClick = {addInput}>+</Button>
            
          </div>
          <div className = 'fila'>
            <Button handleClick = {addInput}>4</Button>
            <Button handleClick = {addInput}>5</Button>
            <Button handleClick = {addInput}>6</Button>
            <Button handleClick = {addInput}>-</Button>

          </div>
          <div className = 'fila'>
            <Button handleClick = {addInput}>7</Button>
            <Button handleClick = {addInput}>8</Button>
            <Button handleClick = {addInput}>9</Button>
            <Button handleClick = {addInput}>*</Button>
          </div>
          <div className = 'fila'>
            <Button handleClick = {getResult}>=</Button>
            <Button handleClick = {addInput}>0</Button>
            <Button handleClick = {addInput}>.</Button>
            <Button handleClick = {addInput}>/</Button>
          </div>
          <div className = 'fila'>
            <ClearButton handleClear={() => setInput('')}>Clear</ClearButton>
          </div>
        </div>
    </div>

  );

}

export default App;
