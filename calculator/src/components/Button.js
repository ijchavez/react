import React from 'react';
import '../Styles/button.css';

function Button(props){
    const isOperator = valor =>{
        return isNaN(valor) && (valor !== '.') && (valor !== '=');

    };
    return (
        <div
            className = {`container-button ${isOperator(props.children) ? 'operator' : '' }`.trimEnd()}
            onClick={() => props.handleClick(props.children)}>
            {props.children}

        </div> 
    )
}

export default Button;