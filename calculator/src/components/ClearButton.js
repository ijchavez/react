import React from 'react';
import '../Styles/clearButton.css';

const clearButton = (props) => (
    <div className = "clear-button"
         onClick = {() => props.handleClear()}>
         {props.children}

    </div>

);

export default clearButton;